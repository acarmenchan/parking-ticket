#ifndef FILEMANAGER_H
#define FILEMANAGER_H
#include "ParkingMeter.h"
#include "PoliceOfficer.h"
#include "ParkingTicket.h"
#include <fstream>
#include <string>
using namespace std;

class FileManager
{
private:
	ofstream fout;
	ifstream fin;

public:
	FileManager(string outputFileName, string inputFileName);	// constructor
	~FileManager();		// destructor
    string readFirstWord();
    int getEnterExitMeterNumber();
    void readEnterLine(ParkingMeter &parkingMeter);
    void readExitLine(ParkingMeter &parkingMeter, PoliceOfficer &policerOfficerOnDuty);
    void readInLine(PoliceOfficer &policeOfficerOnDuty);
    void readOutLine();
};

#endif 