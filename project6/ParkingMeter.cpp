#include "ParkingMeter.h"
#include "ParkingTicket.h"
#include "FileManager.h"
#include <iostream>
#include <math.h>
using namespace std;

ParkingMeter::ParkingMeter()
{
	
}

ParkingMeter::~ParkingMeter()
{
    
}

ParkingTicket ParkingMeter::exitCar(PoliceOfficer &policeOfficerOnDuty)
{
    float parkedMinutes = ((parkedCar.getParkingTimeOut() - parkedCar.getParkingTimeIn())/60);
    
    
    ParkingTicket parkingTicket;
    parkingTicket.setFine(0.0);
    if (paidMinutes < parkedMinutes)
    {
        int overTime = parkedMinutes - paidMinutes;
        double chargeHours = (overTime/60.0);
        int fine = 0;
        int firstHourFee = 25;
        int subsequentHourFee = 10;
        
        if (chargeHours > 1)
            fine = firstHourFee + ((ceil(chargeHours-1)) * subsequentHourFee);
        if (chargeHours <= 1)
            fine = firstHourFee;
        cout << "The Fine is " << fine << endl;
        parkingTicket.setFine(fine);
        parkingTicket.setMake(parkedCar.getMake());
        parkingTicket.setModel(parkedCar.getModel());
        parkingTicket.setColor(parkedCar.getColor());
        parkingTicket.setLicenseNumber(parkedCar.getLicenseNumber());
        parkingTicket.setOfficerName(policeOfficerOnDuty.getOfficerName());
        parkingTicket.setOfficerBadgeNumber(policeOfficerOnDuty.getOfficerBadgeNumber());
    }

    return parkingTicket;
}

ostream& operator<<(ostream &output, ParkingMeter &parkingMeter)
{
    output << "PARKING METER DISPLAY" << endl;
    output << "======================" << endl;
    output << "Spot Number: " << parkingMeter.getSpotNumber() << endl;
    output << "Paid Minutes: " << parkingMeter.getPaidMinutes() << endl;
    output << parkingMeter.getParkedCar() << endl;
    
    return output;
}

