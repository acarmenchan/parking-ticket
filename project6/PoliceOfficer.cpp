#include "PoliceOfficer.h"
#include <iostream>
using namespace std;

PoliceOfficer::PoliceOfficer()
{
	
}

PoliceOfficer::~PoliceOfficer()
{
    
}


ostream& operator<<(ostream &output, const PoliceOfficer &policeOfficer)
{
    output << "POLICE OFFICER DISPLAY" << endl;
    output << "======================" << endl;
    output << "Name: " << policeOfficer.getOfficerName() << endl;
    output << "Badge No: " << policeOfficer.getOfficerBadgeNumber() << endl;
    output << "Clock In: " << policeOfficer.getOfficerClockIn() << endl;
    
    return output;
}
