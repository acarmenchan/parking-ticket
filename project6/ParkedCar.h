#ifndef PARKEDCAR_H
#define PARKEDCAR_H
#include <string>
#include <time.h>
using namespace std;


class ParkedCar
{
    private:
    string make;
    string model;
    string color;
    string licenseNumber;
    time_t parkingTimeIn;
    time_t parkingTimeOut;
    
    public:
    // constructor
    ParkedCar();
    ~ParkedCar();
    
    // mutator functions
    void setMake(string _make)
    {   make = _make;   }
    
    void setModel(string _model)
    {   model = _model; }
    
    void setColor(string _color)
    {   color = _color; }
    
    void setLicenseNumber(string _licenseNumber)
    {   licenseNumber = _licenseNumber;  }
    
    void setParkingTimeIn(time_t _parkingTimeIn)
    {   parkingTimeIn = _parkingTimeIn; }
    
    void setParkingTimeOut(time_t _parkingTimeOut)
    {   parkingTimeOut = _parkingTimeOut;    }
    
    // accessor functions
    string getMake() const
    {   return make;    }
    
    string getModel() const
    {   return model;   }
    
    string getColor() const
    {   return color;   }
    
    string getLicenseNumber() const
    {   return licenseNumber;   }
    
    time_t getParkingTimeIn() const
    {   return parkingTimeIn;       }
    
    time_t getParkingTimeOut() const
    {   return parkingTimeOut;      }
    
    friend ostream& operator<<(ostream &output, const ParkedCar &parkedCar);

    

};

#endif
