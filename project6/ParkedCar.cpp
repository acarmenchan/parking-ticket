#include "ParkedCar.h"
#include <iostream>
using namespace std;


ParkedCar::ParkedCar()
{
	
}

ParkedCar::~ParkedCar()
{
    
}

ostream& operator<<(ostream &output, const ParkedCar &parkedCar)
{
    output << "Make: " << parkedCar.getMake() << endl;
    output << "Model: " << parkedCar.getModel() << endl;
    output << "Color: " << parkedCar.getColor() << endl;
    output << "License Number: " << parkedCar.getLicenseNumber() << endl;
    output << "Parking Time In: " << parkedCar.getParkingTimeIn() << endl;
    output << "Parking Time Out: " << parkedCar.getParkingTimeOut() << endl;
    
    return output;
}