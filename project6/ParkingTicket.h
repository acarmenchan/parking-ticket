#ifndef PARKINGTICKET_H
#define PARKINGTICKET_H
#include <string>
using namespace std;

class ParkingTicket
{
    private:
    string make;
    string model;
    string color;
    string licenseNumber;
    double fine;
    string officerName;
    unsigned int officerBadgeNumber;


    public:
    ParkingTicket();
    ~ParkingTicket();
    
    void setMake(string _make)
    {   make = _make;   }
    
    void setModel(string _model)
    {   model = _model; }
    
    void setColor(string _color)
    {   color = _color; }
    
    void setLicenseNumber(string _licenseNumber)
    {   licenseNumber = _licenseNumber;     }
    
    void setOfficerName(string _officerName)
    {   officerName = _officerName;     }
    
    void setFine(double _fine)
    {   fine = _fine;   }
    
    void setOfficerBadgeNumber(unsigned int _officerBadgeNumber)
    {   officerBadgeNumber = _officerBadgeNumber;   }
    
    
    
    string getMake() const
    {   return make;    }
    
    string getModel() const
    {   return model;   }
    
    string getColor() const
    {   return color;   }
    
    string getLicenseNumber() const
    {   return licenseNumber;   }
    
    string getOfficerName() const
    {   return officerName;     }
    
    double getFine() const
    {   return fine;    }
    
    unsigned int getOfficerBadgeNumber() const
    {   return officerBadgeNumber;      }
    
    friend ostream& operator<<(ostream &output, const ParkingTicket &parkingTicket);

};

#endif
