#ifndef POLICEOFFICER_H
#define POLICEOFFICER_H
#include <iostream>
#include <string>
#include <time.h>
using namespace std;

class PoliceOfficer
{
    private:
    string officerName;
    unsigned int officerBadgeNumber;
    time_t officerClockIn;

    public:
    PoliceOfficer();
    ~PoliceOfficer();
    
    void setOfficerName(string _officerName)
    {   officerName = _officerName;     }
    
    void setOfficerBadgeNumber(unsigned int _officerBadgeNumber)
    {   officerBadgeNumber = _officerBadgeNumber;   }
    
    void setOfficerClockIn(time_t _officerClockIn)
    {   officerClockIn = _officerClockIn;     }
    
    string getOfficerName() const
    {   return officerName;     }
    
    unsigned int getOfficerBadgeNumber() const
    {   return officerBadgeNumber;  }
    
    time_t getOfficerClockIn() const
    {   return officerClockIn;     }
    
    friend ostream& operator<<(ostream &output, const PoliceOfficer &policeOfficer);
    
    
    
};

#endif
