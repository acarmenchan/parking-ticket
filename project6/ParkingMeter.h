#ifndef PARKINGMETER_H
#define PARKINGMETER_H
#include "ParkedCar.h"
#include "PoliceOfficer.h"
#include "ParkingTicket.h"

class ParkingMeter
{
    private:
    int spotNumber;
    int paidMinutes;
    ParkedCar parkedCar;

    public:
    ParkingMeter();
    ~ParkingMeter();
    
    void setSpotNumber(int _spotNumber)
    {   spotNumber = _spotNumber;   }
    
    void setPaidMinutes(int _paidMinutes)
    {   paidMinutes = _paidMinutes; }
    
    void setParkedCar(ParkedCar _parkedCar)
    {   parkedCar = _parkedCar; }
    
    int getSpotNumber() const
    {   return spotNumber;  }
    
    int getPaidMinutes() const
    {   return paidMinutes; }
    
    ParkedCar& getParkedCar()
    {   return parkedCar;   }
    
    ParkingTicket exitCar(PoliceOfficer &policeOfficerOnDuty);

    friend ostream& operator<<(ostream &output, ParkingMeter &parkingMeter);
    
};

#endif
