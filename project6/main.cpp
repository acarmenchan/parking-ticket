// Carmen Chan
// CS 1337.001
// Mr. Smith
// csc130530

#include "FileManager.h"
#include "ParkingMeter.h"
#include "ParkedCar.h"
#include "PoliceOfficer.h"
#include <iostream>
#include <fstream>
#include <string>

using namespace std;


// Files names
const string outputFileName = "Tickets";
const string inputFileName = "Parking";


int main()
{
    
    FileManager fm(outputFileName, inputFileName);
 
    ParkingMeter parkingMeters[10];
    
    PoliceOfficer policeOfficerOnDuty;
    
    for (int x = 0; x < 10; x++)
        parkingMeters[x].setSpotNumber(x + 1);
    
    string firstWord = fm.readFirstWord();
    int lineCount = 1;
    
    while (!firstWord.empty())
    {
        cout << "Reading line " << lineCount << endl;
        
        if (firstWord == "enter")
        {
            int meterNumber = (fm.getEnterExitMeterNumber() - 1);
            
            fm.readEnterLine(parkingMeters[meterNumber]);
            cout << "CAR ENTER" << endl << parkingMeters[meterNumber];
        }
        else if (firstWord == "exit")
        {
            int meterNumber = (fm.getEnterExitMeterNumber() - 1);
            
            fm.readExitLine(parkingMeters[meterNumber], policeOfficerOnDuty);
            cout << "CAR EXIT" << endl << parkingMeters[meterNumber];
            
            
        }
        else if (firstWord == "in")
        {
            fm.readInLine(policeOfficerOnDuty);
            cout << "POLICE OFFICER IN" << endl << policeOfficerOnDuty;
        }
        else if (firstWord == "out")
        {
            fm.readOutLine();
        }
        
        lineCount++;
        
        firstWord = fm.readFirstWord();
    }
    
//    cout << parkingMeters[9];
//    cout << parkingMeters[9];
 	
//	ParkingMeter* pmPtr = fm.readData();		// Read file and initialize

    return 0;
}