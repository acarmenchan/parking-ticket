#include "ParkingTicket.h"
#include <iostream>
using namespace std;

ParkingTicket::ParkingTicket()
{
	
}

ParkingTicket::~ParkingTicket()
{
    
}


ostream& operator<<(ostream &output, const ParkingTicket &parkingTicket)
{
    output << "Make: " << parkingTicket.getMake() << endl;
    output << "Model: " << parkingTicket.getModel() << endl;
    output << "Color: " << parkingTicket.getColor() << endl;
    output << "License Number: " << parkingTicket.getLicenseNumber() << endl;
    output << "Fine: " << parkingTicket.getFine() << endl;
    output << "Officer: " << parkingTicket.getOfficerName() << endl;
    output << "Badge Number: " << parkingTicket.getOfficerBadgeNumber() << endl;
    output << endl;
    
    return output;
}