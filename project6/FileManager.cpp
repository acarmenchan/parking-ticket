#include "FileManager.h"
#include "PoliceOfficer.h"
#include <string>
#include <cstring>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <time.h>
#include <iostream>
#include "ParkedCar.h"

using namespace std;

FileManager::FileManager(string outputFileName, string inputFileName)
{
	fin.open(inputFileName + ".dat");
	fout.open(outputFileName + ".txt");
}


FileManager::~FileManager()
{
	fin.close();
	fout.close();
}

string FileManager::readFirstWord()
{
    string firstWord = "";
    
    fin >> firstWord;
    return firstWord;
}

int FileManager::getEnterExitMeterNumber()
{
    string spotNumber;
    fin >> spotNumber;
    
    return atoi(spotNumber.c_str());
}

void FileManager::readEnterLine(ParkingMeter &parkingMeter)
{
    // input variables
    int parkingTimeHourIn;
    char colonDummy;
    int parkingTimeMinuteIn;
    string make;
    string model;
    string color;
    string licenseNumber;
    int paidTime;
    
    fin >> parkingTimeHourIn;
    fin >> colonDummy;
    fin >> parkingTimeMinuteIn;
    
    // referenced from cplusplus.com/reference/ctime/localtime
    time_t currentTime = time(NULL); // get current time
    struct tm parkingTime = *(localtime(&currentTime)); // initializing tm structure with current time components
    
    parkingTime.tm_min = parkingTimeMinuteIn;
    parkingTime.tm_hour = parkingTimeHourIn;
    parkingTime.tm_sec = 0;
    
    parkingMeter.getParkedCar().setParkingTimeIn(mktime(&parkingTime));
    
    parkingMeter.getParkedCar().setParkingTimeOut(0);
    
    fin >> make;
    parkingMeter.getParkedCar().setMake(make);
    
    fin >> model;
    parkingMeter.getParkedCar().setModel(model);

    fin >> color;
    parkingMeter.getParkedCar().setColor(color);
    
    fin >> licenseNumber;
    parkingMeter.getParkedCar().setLicenseNumber(licenseNumber);
    
    fin >> paidTime;
    parkingMeter.setPaidMinutes(paidTime);
}

void FileManager::readExitLine(ParkingMeter &parkingMeter, PoliceOfficer &policeOfficerOnDuty)
{
    int parkingTimeHourOut;
    char colonDummy;
    int parkingTimeMinuteOut;
    
    fin >> parkingTimeHourOut;
    fin >> colonDummy;
    fin >> parkingTimeMinuteOut;
    
    time_t currentTime = time(NULL); // get current time
    struct tm parkingTime = *(localtime(&currentTime)); // initializing tm structure with current time components
    
    parkingTime.tm_min = parkingTimeMinuteOut;
    parkingTime.tm_hour = parkingTimeHourOut;
    parkingTime.tm_sec = 0;
    
    parkingMeter.getParkedCar().setParkingTimeOut(mktime(&parkingTime));
    
    ParkingTicket parkingTicket;
    parkingTicket = parkingMeter.exitCar(policeOfficerOnDuty);
    
    if (parkingTicket.getFine() > 0)
        fout << parkingTicket;
}

void FileManager::readInLine(PoliceOfficer &policeOfficerOnDuty)
{
    string officerName;
    string officerBadgeNumber;
    int officerHourClockIn;
    char colonDummy;
    int officerMinuteClockIn;
    
    fin >> officerName;
    policeOfficerOnDuty.setOfficerName(officerName);
    
    fin >> officerBadgeNumber;
    policeOfficerOnDuty.setOfficerBadgeNumber(atoi(officerBadgeNumber.c_str()));
    
    fin >> officerHourClockIn;
    fin >> colonDummy;
    fin >> officerMinuteClockIn;
    
    time_t currentTime = time(NULL); // get current time
    struct tm officerClockIn = *(localtime(&currentTime)); // initializing tm structure with current time components
    
    officerClockIn.tm_min = officerMinuteClockIn;
    officerClockIn.tm_hour = officerHourClockIn;
    officerClockIn.tm_sec = 0;
    
    policeOfficerOnDuty.setOfficerClockIn(mktime(&officerClockIn));
    
}

void FileManager::readOutLine()
{
    string name;
    int numOne;
    char colonDummy;
    int numTwo;
    
    fin >> name >> numOne >> colonDummy >> numTwo;
    
    
}